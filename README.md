# README #

A very simple console version of Battleship, written by Clifton Dobrich. As source code is copyright Clifton Dobrich. Please do not copy or modify this code without permission and also please do give notice and credit.

### What is this repository for? ###

* This is my own implementation of the game Battleship, with simple limitations and very simple custom console text graphics. I made this as a demonstration of my coding abilities for a job interview over the course of several hours.
* Version 1.0: Complete with 
* Limitations: The board game size may range between 10x10 and 26x26, inclusively.

### How do I get set up? ###

* Set up: There is an intellij project file preset with this program. But in general all you need to do is try to compile the 'src/Main.java' file and the rest will follow.
* Configuration: This program uses simple pure Java to run. It should be compatible with Java versions 6, 7, 8 and 9. But it was built using version 8. The main program uses only pure Java.
* Dependencies: The Unit tests are built with JUnit and that is the only dependency if you want to run them. They should be included in your system path.
* How to run the tests: The 'tests' folder has JUnit tests for each class and its various methods. They can be invoked independently to test functionality.

### Contribution guidelines ###

I'm not generally looking for code contributions but I am willing to listen. The original purpose of this project was simply to demonstrate my ability to quickly create a fully working project with a neat and organized manner.

### Who do I talk to? ###

* Repo owner: This project and all its source code files and accompanying files are owned by Clifton Dobrich. 